package android.example.fisiapp;

import android.content.Intent;

import android.example.fisiapp.persistencia.AppExecutors;
import android.example.fisiapp.persistencia.EjercicioDatabase;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailEjercicio extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_diario);

//        DocumentReference mDocRef;
//
//        Map<String, Object> mapita = new HashMap<>();
//        mapita.put("nombre", "Sentadillas");
//        mapita.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita.put("tipo", "Sentadillas");
//        mapita.put("repeticiones", 10);
//        mapita.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita.put("completitud", false);
//
//        for (int pos = 0; pos < 10; pos++) {
//            mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document("ejercicio" + pos);
//
//            mDocRef.set(mapita).addOnSuccessListener(new OnSuccessListener<Void>() {
//                @Override
//                public void onSuccess(Void aVoid) {
//                    Log.d("respuesta", "Documento Guardado");
//                }
//            }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    Log.w("respuesta", "hubo un error", e);
//                }
//            });
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_ejercicio, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()){
            case R.id.home:
                startActivity(new Intent(this, HomeActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
