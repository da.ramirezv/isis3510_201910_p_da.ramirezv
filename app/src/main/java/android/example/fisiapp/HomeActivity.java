package android.example.fisiapp;

import android.example.fisiapp.detailhistorial.DetailHistorialFragment;
import android.example.fisiapp.historial.HistorialFragment;
import android.example.fisiapp.listadiaria.HomeFragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class HomeActivity extends AppCompatActivity implements DetailHistorialFragment.OnFragmentInteractionListener, HomeFragment.OnFragmentInteractionListener , HistorialFragment.OnFragmentInteractionListener{

    public static String NOMBRE_BUNDLEAC = "NOMBRE_BUNDLE";
    public static Bundle INFO_BUNDLEAC;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_ejercicios:
                    HomeFragment homeFragment = new HomeFragment();
                    FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
                    transition.replace(R.id.contenedor,homeFragment);
                    transition.commit();
                    return true;
                case R.id.navigation_historial:
                    HistorialFragment historialFragment = new HistorialFragment();
                    FragmentTransaction transition2 = getSupportFragmentManager().beginTransaction();
                    transition2.replace(R.id.contenedor,historialFragment);
                    transition2.commit();
                    return true;
                case R.id.navigation_notifications:

                    return true;
            }
            return false;
        }
    };

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionstoolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        HomeFragment homeFragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.contenedor, homeFragment).commit();

        // Create a new user with a first and last name

//        mDocRef.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
//            @Override
//            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
//                if(documentSnapshot.exists())
//                {
//                    Log.d("respuesta", "");
//                    String nombre = documentSnapshot.getString("Nombre");
//                    String descripcion = documentSnapshot.getString("Descripcion");
//                    String tipo = documentSnapshot.getString("Tipo");
//                    Long repeticiones = (Long) documentSnapshot.get("Repeticiones");
//
//                }
//                else if (e!= null)
//                {
//                    Log.w("respuesta", "hubo un error", e);
//                }
//            }
//        });
//        mDocRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//            @Override
//            public void onSuccess(DocumentSnapshot documentSnapshot) {
//                if(documentSnapshot.exists())
//                {
//                    String first = documentSnapshot.getString("first");
//                    String last = documentSnapshot.getString("last");
//                    String born = documentSnapshot.getString("born");
//                    Log.d("real bro", "" + first + last + born);
//                }
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            // do something here

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onSaveInstanceState (Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putBundle(NOMBRE_BUNDLEAC, INFO_BUNDLEAC);
    }
}
