package android.example.fisiapp.listadiaria;

import android.example.fisiapp.R;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;

public class HomeAdaptador extends RecyclerView.Adapter<HomeAdaptador.NumberViewHolder>  {

    private static final String TAG = HomeAdaptador.class.getSimpleName();
    private final ListItemClickListener mOnClickListener;
    private int mNumberItems;
    public Bundle arreglo = new Bundle();


    public interface ListItemClickListener{
        void onListItemClick(int clickedItemIndex);
    }

    public HomeAdaptador(int numberOfItems, ListItemClickListener listener) {
        mNumberItems = numberOfItems;
        mOnClickListener = listener;
    }

    @Override
    public NumberViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType){
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.recycler_ejercicios;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        NumberViewHolder viewHolder = new NumberViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NumberViewHolder holder, int position) {
        Log.d(TAG, "#" + position);
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    public void setArreglo(Bundle parreglo){
        arreglo = parreglo;
        notifyDataSetChanged();
    }

    class NumberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView titulo;
        TextView tipo;
        ImageView completo;

        public NumberViewHolder(View itemView){
            super(itemView);

            titulo = (TextView) itemView.findViewById(R.id.tituloejercicio);
            tipo = (TextView) itemView.findViewById(R.id.tipoejercicio);
            completo = (ImageView) itemView.findViewById(R.id.comprobacion);
            itemView.setOnClickListener(this);
        }

        void bind()
        {
            try
            {
                for (String key : arreglo.keySet()) {

                    InfoEjercicioPojo temp = (InfoEjercicioPojo) arreglo.get(key);
                    titulo.setText(temp.getNombre());
                    Log.d("yolo", temp.getNombre());
                    tipo.setText(temp.getTipo());
                    if(temp.isCompletitud())
                    {
                        Log.d("eluno","wtf");
                        completo.setImageResource(R.drawable.ic_iconmonstr_check_mark_thin);
                    }
                    else
                    {
                        Log.d("eldos","wtf");
                        completo.setImageResource(R.drawable.ic_iconmonstr_x_mark_thin);
                    }
                }

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition);

        }
    }
}