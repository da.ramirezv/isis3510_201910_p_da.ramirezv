package android.example.fisiapp.listadiaria;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.example.fisiapp.DetailEjercicio;
import android.example.fisiapp.R;
import android.example.fisiapp.persistencia.AppExecutors;
import android.example.fisiapp.persistencia.EjercicioDatabase;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.example.fisiapp.persistencia.MainViewModel;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements HomeAdaptador.ListItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private HomeAdaptador mAdapter;
    private RecyclerView mNumberList;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public EjercicioDatabase mDb;
    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("prueba random1", "nice");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDb = EjercicioDatabase.getInstance(getActivity().getApplicationContext());
        final View view = inflater.inflate(R.layout.fragment_home, container, false);

        DocumentReference mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1");
        final Bundle arreglo = new Bundle();
        final ArrayList<InfoEjercicioPojo> listaarreglo = new ArrayList<>();

        final ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting() || mobile.isConnectedOrConnecting()) {

            mDocRef.collection("Ejercicios")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                               @Override
                                               public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                                   if (task.isSuccessful()) {
                                                       for (QueryDocumentSnapshot document : task.getResult()) {
                                                           String nombre = document.getString("nombre");
                                                           String descripcion = document.getString("descripcion");
                                                           String tipo = document.getString("tipo");
                                                           Long repeticiones = (Long) document.get("repeticiones");
                                                           Long creacion = (Long) document.get("creacion");
                                                           Boolean completitud = (Boolean) document.getBoolean("completitud");
                                                           InfoEjercicioPojo pojoTemp = new InfoEjercicioPojo(nombre, descripcion, tipo, repeticiones, new Date(creacion), completitud);
                                                           listaarreglo.add(pojoTemp);
                                                           if(DateUtils.isToday(pojoTemp.getCreacion().getTime()))
                                                           {
                                                               arreglo.putSerializable(document.getId(), pojoTemp);
                                                           }
                                                       }
                                                       checkearInsertar(listaarreglo);
                                                       mNumberList = (RecyclerView) view.findViewById(R.id.recycler);
                                                       LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                                       mNumberList.setLayoutManager(layoutManager);
                                                       mNumberList.setHasFixedSize(true);
                                                       mAdapter = darAdaptador(arreglo);
                                                       mAdapter.setArreglo(arreglo);
                                                       mNumberList.setAdapter(mAdapter);

                                                   } else {
                                                       Log.d("prueba1", "Error getting documents: ", task.getException());
                                                   }
                                               }
                                           }
                    );

        } else {
            mNumberList = (RecyclerView) view.findViewById(R.id.recycler);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            mNumberList.setLayoutManager(layoutManager);
            mNumberList.setHasFixedSize(true);
            mAdapter = darAdaptador(arreglo);
            mAdapter.setArreglo(arreglo);
            mNumberList.setAdapter(mAdapter);
            retrieveTasks();
        }
        return view;
    }


    private void retrieveTasks() {
        final MainViewModel viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        viewModel.getTasks().observe(this, new Observer<List<InfoEjercicioPojo>>() {
            @Override
            public void onChanged(@Nullable List<InfoEjercicioPojo> entries) {
                Log.d("respuesta", "Receiving database update from LiveData");
                Bundle nuevo = new Bundle();
                int pos = 0;
                for (InfoEjercicioPojo temp : entries) {
                    Log.d("respuesta222", ""+pos);
                    if(DateUtils.isToday(temp.getCreacion().getTime()))
                    {
                        Log.d("respuesta555", ""+pos);
                        nuevo.putSerializable("ejercicios" + pos, temp);
                        pos++;
                    }
                }
                mAdapter = darAdaptador(nuevo);
                mAdapter.setArreglo(nuevo);
                mNumberList.setAdapter(mAdapter);
            }
        });
        viewModel.getTasks().removeObservers(getActivity());
    }

    public void checkearInsertar(final List<InfoEjercicioPojo> plist){
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.ejercicioDao().nukeTable();
                for (InfoEjercicioPojo temp: plist) {
                    mDb.ejercicioDao().insertEjercicio(temp);
                }
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public HomeAdaptador darAdaptador(Bundle bundle) {
        HomeAdaptador temp = new HomeAdaptador(bundle.size(), this);
        return temp;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {
//        startActivity(new Intent(getActivity(), DetailEjercicio.class));
    }
}
