package android.example.fisiapp.historial;

import android.example.fisiapp.persistencia.InfoEjercicioPojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class HistorialPojo implements Serializable {
    private String fecha;
    private boolean completo = true;
    private ArrayList<InfoEjercicioPojo> ejercicios = new ArrayList<>();

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public boolean isCompleto() {
        return completo;
    }

    public void establecerCompleto() {

        for (InfoEjercicioPojo info: ejercicios) {
            if(!info.isCompletitud())
            {
                completo = false;
                return;
            }
        }
    }

    public ArrayList<InfoEjercicioPojo> getEjercicios() {
        return ejercicios;
    }

    public void setEjercicios(ArrayList<InfoEjercicioPojo> ejercicios) {
        this.ejercicios = ejercicios;
    }

    public void addEjercicio(InfoEjercicioPojo ejercicio) {
        this.ejercicios.add(ejercicio);
    }

}
