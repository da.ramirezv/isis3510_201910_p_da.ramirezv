package android.example.fisiapp.historial;

import android.example.fisiapp.R;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;

import java.util.ArrayList;

public class HistorialAdaptador extends RecyclerView.Adapter<HistorialAdaptador.NumberViewHolder>  {

    private static final String TAG = HistorialAdaptador.class.getSimpleName();
    private final ListItemClickListener mOnClickListener;
    private int mNumberItems;
    public Bundle arreglo = new Bundle();


    public interface ListItemClickListener{
        void onListItemClick(int clickedItemIndex, HistorialPojo historialPojo);
    }

    public HistorialAdaptador(int numberOfItems, ListItemClickListener listener) {
        mNumberItems = numberOfItems;
        mOnClickListener = listener;
    }

    @Override
    public NumberViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType){
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.recycler_historial;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        NumberViewHolder viewHolder = new NumberViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NumberViewHolder holder, int position) {
        Log.d(TAG, "#" + position);
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    public void setArreglo(Bundle parreglo){
        arreglo = parreglo;
        notifyDataSetChanged();
    }

    class NumberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView fecha;
        ImageView completo;
        ArrayList<HistorialPojo> arregloHistoriales = new ArrayList<>();

        public NumberViewHolder(View itemView){
            super(itemView);

            fecha = (TextView) itemView.findViewById(R.id.fecha);
            completo = (ImageView) itemView.findViewById(R.id.completitud);
            itemView.setOnClickListener(this);
        }

        void bind()
        {
            try
            {
                for (String key : arreglo.keySet()) {

                    HistorialPojo temp = (HistorialPojo) arreglo.get(key);
                    arregloHistoriales.add(temp);
                    fecha.setText(temp.getFecha());
                    if(temp.isCompleto())
                    {
                        Log.d("eluno","wtf");
                        completo.setImageResource(R.drawable.ic_iconmonstr_check_mark_thin);
                    }
                    else
                    {
                        Log.d("eldos","wtf");
                        completo.setImageResource(R.drawable.ic_iconmonstr_x_mark_thin);
                    }
                }

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition, arregloHistoriales.get(clickedPosition));

        }
    }
}